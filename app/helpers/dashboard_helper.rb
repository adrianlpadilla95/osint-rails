module DashboardHelper
  def validate_usr(username)
    username.gsub!("https://www.facebook.com/", "")
    !!username[/^([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)$/]
  end

  def get_username(username)
    username.gsub!("https://www.facebook.com/", "")
    username
  end
end
