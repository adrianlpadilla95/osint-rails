class PeopleController < ApplicationController
  def create
    @person = current_user.people.new(person_params)
    
    respond_to do |format|
      if @person.save
        format.html { redirect_to root_path, notice: 'La persona fue creada' }
      else
        format.html { redirect_to root_path, alert: @person.errors}
      end
    end
  end

  def search_people
    respond_to do |format|
      if @people = Person.where('nombre LIKE ? AND apodo LIKE ?',
                                "%#{params[:nombre]}%",
                                "%#{params[:apodo]}%").limit(10)
        format.js
        format.json { render json: @people }
      end
    end
  end

  def search_facebook_detail
    respond_to do |format|
      if @detail = get_facebook_detail(params[:link])
        format.js
        format.json { render json: @detail }
      end
    end
  end

  private 
  def person_params
    params.require(:person).permit(:nombre, :apodo, :created_by_id, :modified_by_id)
  end
end
