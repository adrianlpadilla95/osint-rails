class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  layout :user_layout

  
  #############################################################################################
  
  require 'nokogiri'
  require 'open-uri'
  require 'json'
  require 'openssl'
  
  private

  # Get doc HTML for parser
  def get_doc(url)
    doc = Nokogiri::HTML(open(url))
    doc.xpath("//script").remove
    doc.xpath("//style").remove
    doc.xpath('//comment()').each { |comment| comment.replace(comment.text) }
    doc
  end

  def get_list_social(nickname)
    socials = {
      github: "https://github.com/#{nickname}",
      facebook: "https://facebook.com/#{nickname}",
      twitter: "https://twitter.com/#{nickname}",
      instagram: "https://instagram.com/#{nickname}"
    }
    # github = get_doc('url')
    response = []
    response << get_github_detail(socials[:github])
    response << get_twitter_detail(socials[:twitter])
    response
  end

  def get_github_detail(url)
    begin
      doc = get_doc(url)
      result = {}
      doc.css('.h-card').each_with_index do |link, i|
        result[:link] = url
        result[:name] = link.at_css('span.p-name').content
        result[:origen] = 'Github'
        result[:img] = link.at_css('img.avatar')['src']
      end
      result
    rescue
      {}
    end
  end
  
  def get_twitter_detail(url)
    begin
      doc = get_doc(url)
      result = {}
      doc.css('#page-container').each_with_index do |link, i|
        result[:link] = url
        result[:name] = link.at_css('a.ProfileHeaderCard-nameLink').content
        result[:origen] = 'Twitter'
        result[:img] = link.at_css('img.ProfileAvatar-image')['src']
      end
      result
    rescue
      {}
    end
  end

  # Get all people from name
  def get_list_facebook(url)
    doc = get_doc("https://www.facebook.com/public/#{url}")
    response = []
    doc.css('#BrowseResultsContainer>div>div>div>div').each_with_index do |link, i|
      result = {}
      result[:link] = link.at_css('div>div.clearfix>div:nth-child(2n)>div>a')['href']
      result[:origen] = 'Facebook'
      result[:name] = link.at_css('div>div.clearfix>div:nth-child(2n)>div>a').content
      result[:img] = link.at_css('a>img')['src']
      response << result
      break if i == 10
    end
    response
  end

  # Get data person from url
  def get_facebook_detail(url)
    doc = get_doc(url)
    response = {}
    response[:id] = get_fb_id(doc)
    response[:name] = get_name(doc)
    response[:photo] = get_photo(doc)
    response[:places] = get_places(doc)
    response[:academic] = get_studies(doc)
    response[:bio] = get_bio(doc)
    response[:url] = url
    response
  end

  def get_fb_id(doc)
    doc.xpath('/html/head/meta[@property="al:android:url"]/@content').to_s.delete("fb://profile/")
  end

  def get_places(doc)
    response = []
    places_c =[]
    places =[]
    doc.search(".fbProfileEditExperiences li div div div.clearfix div:nth-child(2) div div div.fsm").each{|link| places_c << link.content}
    doc.search(".fbProfileEditExperiences li div div div.clearfix div:nth-child(2) div div span").each{|link| places << link.content}
    places.each_with_index do |place, i| 
      response << { places_c[i] => places[i]}
    end
    response.reject(&:empty?)
  end

  def get_photo(doc)
    id= doc.xpath('/html/head/meta[@property="al:android:url"]/@content').to_s.delete("fb://profile/")
    "https://graph.facebook.com/#{id}/picture?type=large"
  end

  def get_name(doc)
    doc.xpath('/html/head/meta[@property="og:title"]/@content').to_s
  end

  def get_studies(doc)
    response = []
    
    academic ={}
    study = []
    work = []
    doc.search("#pagelet_eduwork>div>div").each do |link|
      academic_title = link.at("span[role=heading]").content
      if(academic_title == "Empleo" || academic_title == "Work")
        link.search("ul.fbProfileEditExperiences li.fbEditProfileViewExperience div div.clearfix div div a").each do |l| 
          work << {academic_title => l.content}
        end
      elsif(academic_title == "Formación académica" || academic_title == "Education") 
        link.search("ul.fbProfileEditExperiences li.fbEditProfileViewExperience div div.clearfix div div a").each do |l| 
          study << {academic_title => l.content}
        end
      end
    end

    response << work
    response << study
    response.reject(&:empty?)
  end

  def get_bio(doc)
    if doc.at_css("#pagelet_bio div ul")
      doc.at_css("#pagelet_bio div ul").content
    end
  end

  protected
  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  private

  def user_layout
    if user_signed_in?
      'application'
    else
      'unauthenticated'
    end
  end


end
