class DashboardController < ApplicationController
  def index
    @person = Person.new
    @people = Person.order(:id => :DESC)
  end

  def facebook
    @people = Person.all.limit(5)
    @detail = Detail.new

    if params[:social]
      nombre = params[:social][:nombre].tr(' ', '-')
      @social_people = get_list_facebook(nombre)
    end
  end

  def nickname
    @people = Person.all.limit(5)
    @detail = Detail.new

    if params[:social]
      nombre = params[:social][:nickname].tr(' ', '-')
      @social_people = get_list_social(nombre)
    end
  end
  
end
