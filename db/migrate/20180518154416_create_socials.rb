class CreateSocials < ActiveRecord::Migration[5.1]
  def change
    create_table :socials do |t|
      t.references :person, foreign_key: true
      t.string :uid
      t.string :foto
      t.string :biografia
      t.integer :tipo

      t.timestamps
    end
  end
end
