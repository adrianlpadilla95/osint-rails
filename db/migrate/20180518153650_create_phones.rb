class CreatePhones < ActiveRecord::Migration[5.1]
  def change
    create_table :phones do |t|
      t.references :person, foreign_key: true
      t.string :telefono
      t.integer :tipo

      t.timestamps
    end
  end
end
