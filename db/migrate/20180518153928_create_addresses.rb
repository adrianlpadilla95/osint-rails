class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.references :person, foreign_key: true
      t.string :direccion
      t.integer :tipo

      t.timestamps
    end
  end
end
