class CreateWorks < ActiveRecord::Migration[5.1]
  def change
    create_table :works do |t|
      t.references :person, foreign_key: true
      t.string :trabajo
      t.datetime :fecha_inicio
      t.datetime :fecha_fin
      t.integer :tipo

      t.timestamps
    end
  end
end
