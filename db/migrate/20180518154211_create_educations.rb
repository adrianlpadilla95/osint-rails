class CreateEducations < ActiveRecord::Migration[5.1]
  def change
    create_table :educations do |t|
      t.references :person, foreign_key: true
      t.string :educacion
      t.datetime :fecha_inicio
      t.datetime :fecha_fin
      t.string :tipo

      t.timestamps
    end
  end
end
