class CreateDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :details do |t|
      t.references :person, foreign_key: true
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :nombres
      t.string :nombre_completo
      t.string :dni
      t.integer :sexo
      t.string :direccion
      t.datetime :fecha_nacimiento

      t.timestamps
    end
  end
end
