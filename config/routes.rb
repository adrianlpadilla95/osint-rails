Rails.application.routes.draw do

  devise_for :users
  resources :people

  get '/facebook', to: 'dashboard#facebook'
  get '/facebook/:nombre', to: 'dashboard#facebook'

  get '/nickname', to: 'dashboard#nickname'
  get '/nickname/:nickname', to: 'dashboard#nickname'

  match 'add_people', to: 'dashboard#add_people', via: [:post]
  match 'search_people', to: 'people#search_people', via: [:post]
  
  match 'search_facebook_detail', to: 'people#search_facebook_detail', via: [:post]
  
  devise_scope :user do
    unauthenticated :user do
      root 'devise/sessions#new'
    end
    
    authenticated :user do
      root 'dashboard#index'
    end
  end


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
